# LaravelKit

After using Vagrant for a while and liking it, I ventured into the Docker world and liked the ease of setting up low-overhead, multi-tiered environments rather than bulky VMs. I was working on a Laravel project at the time, so I set out to build an environment for developing Laravel apps. I found some good work out there already, but some of it was a little dated or built in a way that didn't quite follow the methodology I wanted to use. So I've collected what I felt was the best of all of them to create this. You don't have to like it, your welcome to change it to suit your preferences, this is just how I like setting it up.

# Docker images

The kit includes the source for all the images, so you can adjust it as needed.
1. [Laravel php-fpm](https://hub.docker.com/r/aonz/laravel-fpm/) for running your Laravel application under php-fpm. This is for creating highly scalable apps. This might not be necessary in your case, but since it's easily set up, might as well start that way.
2. [Laravel nginx](https://hub.docker.com/r/aonz/laravel-nginx/) serves up the application to http(s) requests. An Apache 2.4 version is on its way at some point.
3. [Laravel composer](https://hub.docker.com/r/aonz/laravel-composer/) lets you run a specific version of composer without having to install on your local system.
4. [Laravel artisan](https://hub.docker.com/r/aonz/laravel-artisan/) lets you run a specific version of php for artisan without having to install on your local system.
5. [Laravel php-cli](https://hub.docker.com/r/aonz/laravel-php-base/) lets you run a specific version of php-cli without having to install on your local system. Used to build the artisan and composer images from.

# Docker compose
Included is an example docker-compose file, just copy `docker-compose.example` to `docker-compose.yml` in your project directory.

_**NOTE:** You can replace `MYAPP` with whatever you want to name your project folder_
```bash
$ mkdir MYAPP
$ cp /path/to/LaravelKit/docker-compose.example ./MYAPP/docker-compose.yml
```
That's just to keep any passwords out of our git repo. You should probably add `docker-compose.yml` to the `.gitignore` file for your project.

# Getting started
First thing you'll want to do (after you install docker of course) is set up your project.
```bash
$ mkdir -p MYAPP/workspace/www MYAPP/workspace/logs
$ cd MYAPP
$ docker-compose up -d
$ docker-compose run --rm composer create-project laravel/laravel . "5.1.*"
```
_**NOTE:** You can replace `5.1.*` (the current Laravel LTS) with whatever version you want to build your project from, but you may need to upgrade your PHP version. I intentionally chose **LTS** where possible because I'm funny like that. I'd rather spend my weekends surfing or snowboarding._

That last line is one of the cool things I discovered while digging through the documentation out there and other solutions. Let's just say if I had hair, I'd still look the same as I normally do after trying to make this work the way I wanted. Because some of the other solutions are a little dated, they may not have had this functionality. `docker-compose run` lets you run a service defined in your `docker-compose.yml` file with all the environmentals that have been set, rather than using a regular `docker run`, which gets pretty verbose. No bash aliases needed.  

In theory, you should now have a working site (though no database yet).

## Adding the database

Now that we have a running setup, we can add in our database. For the purposes of this setup we're using MySQL, you're welcome to choose another Laravel compatible datastore (eg SQLlite, Postgres or MongoDB).

We'll first make a small change to our docker-compose.yml file to make some environment vars available to our container.

_**NOTE:** now would be a good time to set this project up as a git repository. Remember to add things like the `docker-compose.yml` and `.env` files to your `.gitignore` file_

Add the environmentals to the `db` service in our `docker-compose.yml` file:
```yaml
version: '2'
services:
  db:
    image: mysql:5.6
    restart: always
    environment:
      - "MYSQL_RANDOM_ROOT_PASSWORD=yes"
      - "MYSQL_USER=laravel"
      - "MYSQL_PASSWORD=supersecret"
      - "MYSQL_DATABASE=laravel"
    ports:
      - "33061:3306"
```
The composer create-project command we ran earlier gave us our .env file, we'll want to make sure it lines up with our docker-compose file. There's a bit of Docker networking magic, which it took a bit for me to get my head around, that makes this all hang together later.

The .env file:
```PHP
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_DATABASE=laravel
DB_USERNAME=laravel
DB_PASSWORD=supersecret
```
## Docker networking magic and shared volumes
```yaml
volumes_from:
  - workspace
environment:
  - "DB_PORT=3306"
  - "DB_HOST=db"
```
Okay, it's not magic, it's just so simple that it went over my head a few times before I figured it out. A couple environment settings in our `docker-compose.yml` file, link our `artisan` and `appsrv` services to the `db` service. The shared volume from `workspace` makes sure they're all looking at the same source code. Together, those made all these services magically talk to each other on the right interfaces and ports and read from the same configs and caches.

## Running artisan
Same as the composer command above, this is pretty sweet too.
```
$ docker-compose run --rm artisan migrate
```
That should run the migration for the built in tables for Users. If you don't want them, just roll them back.
```
$ docker-compose run --rm artisan migrate:rollback
```
## Show me the cache
You might also want to run Redis for your cache in Laravel. Not a problem, just need to install one thing and make a couple more edits.
```
$ docker-compose run --rm composer require predis/predis:~1.0
```
Edit your `docker-compose.yml` file to add this:
```yaml

  cache:
    image: redis:3.0
    ports:
      - "63791:6379"
```
and restart docker-compose:

```
$ docker-compose down
$ docker-compose up -d
```        
Now start making cool stuff!
