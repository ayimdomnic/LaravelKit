
This is a PHP 7.0 php-cli [Docker](http://www.docker.com) image. This is intended as a base image for other images like composer and artisan. It's based on the awesome work of [Dylan Lindgren](http://dylanlindgren.com) and [PHPDocker.io](http://phpdocker.io) I just have some different preferences.

1. [Docker for the Laravel framework](http://dylanlindgren.com/docker-for-the-laravel-framework) (24 Sep 2014)

This image works well with the below related images.
- [dylanlindgren/docker-laravel-nginx](https://github.com/dylanlindgren/docker-laravel-nginx)
- [dylanlindgren/docker-laravel-phpfpm](https://github.com/dylanlindgren/docker-laravel-phpfpm)
- [dylanlindgren/docker-laravel-composer](https://github.com/dylanlindgren/docker-laravel-composer)
- [dylanlindgren/docker-laravel-artisan](https://github.com/dylanlindgren/docker-laravel-artisan)
- [dylanlindgren/docker-laravel-bower](https://github.com/dylanlindgren/docker-laravel-bower)
- [dylanlindgren/docker-laravel-phpunit](https://github.com/dylanlindgren/docker-laravel-phpunit)
