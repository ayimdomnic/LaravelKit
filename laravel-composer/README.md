# What's this?
This is a PHP 5.6 php-cli [Docker](http://www.docker.com) image. This is intended as an image to run composer as part of the [LaravelKit](https://gitlab.com/czechbox).


## Credit where it's due
It's based on the awesome work of [Dylan Lindgren](http://dylanlindgren.com/docker-for-the-laravel-framework), [Kyle Ferguson](https://kyleferg.com/laravel-development-with-docker/) and [PHPDocker.io](http://phpdocker.io). I've built it all on the official [Ubuntu 14.04 - Trusty Tahr](https://hub.docker.com/_/ubuntu/) base image (because that's my preference) and re-jigged a quite a few things to update them for 2017. Yes, a Xenial version is coming.
